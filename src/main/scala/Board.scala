package io.scalac.chess

case class Board(row: Int, col: Int) {

  if(row < 0 || col < 0)
    throw new IllegalArgumentException()

  def this(dim: Int) {
    this(dim, dim)
  }

  lazy val allPositions: List[Position] = {
    for {
      x <- List.range(0, row)
      y <- List.range(0, col)
    } yield Position(x, y)
  }

  lazy val empty = Vector.fill(row)(Vector.fill(col)("*"))

}

object Board {
  val pattern = """(\d+)(X|x)(\d+)""".r

  def apply(dim: String)(implicit errorHandler: ErrorHandler): Board = {
    dim match {
      case pattern(row, x, col) ⇒
        new Board (row.toInt, col.toInt)
      case _ ⇒
        errorHandler.handle("Incorrect board dimension format!")
        new Board(0,0)
    }
  }
}
