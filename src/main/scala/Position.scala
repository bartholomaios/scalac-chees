package io.scalac.chess

case class Position(row: Int, col: Int)
