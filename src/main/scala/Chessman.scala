package io.scalac.chess

import io.scalac.chess.figure.Figure

case class Chessman(position: Position, figure: Figure) {

  /**
   * @param chessboard actual chessboard
   * @return true if safe
   */
  def isNoConflict(chessboard: Chessboard): Boolean =
    chessboard.forall(cm => cm.figure.fieldOfDestruction(cm.position)(position) && figure.fieldOfDestruction(position)(cm.position))

  /**
   * give me a safe positions for current position of Figure
   * @return
   */
  def safePositions(positions: List[Position]): List[Position] = {
    positions.filter(figure.fieldOfDestruction(position))
  }

}