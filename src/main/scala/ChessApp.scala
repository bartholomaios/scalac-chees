package io.scalac.chess

import io.scalac.chess.figure.Figure

/**
 * first param should be a string rowXcol eg. '4X5' or '4x5'
 * second param should be a string list of figure eg. 'KQN' or 'KqN'
 */
object ChessApp extends App {

  implicit val errorHandler = new ErrorHandler()

  if(args.size != 2)
    errorHandler.handle("Incorrect amount of arguments!")
  else {
    val chessmen = Figure.translate(args(1))
    val board = Board(args(0))

    errorHandler.validate
    val chess = new Chess(board)
    chess.chessboard(chessmen).map(cb => println(chess.show(cb)))
  }
}


