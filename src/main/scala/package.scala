package io.scalac



package object chess {
  type Chessboard = Set[Chessman]
  object Chessboard {
    val empty: Chessboard = Set()
  }
}