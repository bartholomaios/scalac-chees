package io.scalac.chess

import scala.collection.mutable

class ErrorHandler {
  val helpMessage =
    """first param should be a string rowXcol eg. '4X5' or '4x5'
      |second param should be a string list of figure eg. 'KQN' or 'KqN'
      |
      |Allowed letters
      |K = King
      |Q = Queen
      |B = Bishop
      |N = Knight
      |R = Rook
      |""".stripMargin

  val errors: mutable.ListBuffer[String] = mutable.ListBuffer()

  def handle(message: String): Unit = errors += message

  def isDefined = errors.nonEmpty

  def validate: Unit = if (isDefined) {
    println(s"${errors.mkString("[ERROR] ", "\n[ERROR] ", "\n")}$helpMessage")
    sys.exit(1)
  }

}
