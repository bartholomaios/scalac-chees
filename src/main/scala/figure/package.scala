package io.scalac.chess

package object figure {

  object Figure {
    val all: List[Figure] = King :: Queen :: Bishop :: Knight :: Rook :: Nil
    val allSigns = all.map(_.sign)
    def find(letter: Char): Option[Figure] = all find (_.sign == letter)
    def translate(list:String)(implicit errorHandler: ErrorHandler): List[Figure] = {
      val (known, unknown) = list.partition(allSigns.contains(_))
      if(!unknown.isEmpty) errorHandler.handle(s"Unknown figure: ${unknown.mkString}!")
      known.toList.flatMap(find(_))
    }
  }

  sealed trait Figure {
    val sign: Char

    /**
     * @param pos my position position
     * @return function checking positions are safe
     */
    def fieldOfDestruction(pos: Position): Position => Boolean
  }

  case object King extends Figure {
    val sign = 'K'
    def fieldOfDestruction(pos: Position): Position => Boolean = {
      p: Position =>
        !(math.abs(pos.col - p.col) <= 1 && math.abs(pos.row - p.row) <= 1)
    }
  }

  case object Queen extends Figure {
    val sign = 'Q'
    def fieldOfDestruction(pos: Position): Position => Boolean = {
        p: Position =>
          pos.row != p.row && pos.col != p.col && math.abs(pos.col - p.col) != math.abs(pos.row - p.row)
    }
  }

  case object Bishop extends Figure {
    val sign = 'B'
    def fieldOfDestruction(pos: Position): Position => Boolean = {
        p: Position =>
          math.abs(pos.col - p.col) != math.abs(pos.row - p.row)
    }
  }

  case object Rook extends Figure {
    val sign = 'R'
    def fieldOfDestruction(pos: Position): Position => Boolean = {
         p: Position => pos.col != p.col && pos.row != p.row
    }
  }
  case object Knight extends Figure {
    val sign = 'N'
    def fieldOfDestruction(pos: Position): Position => Boolean = {
      p: Position => {
        val absCol: Int = math.abs(pos.col - p.col)
        val absRow: Int = math.abs(pos.row - p.row)
        !((absCol == 1 && absRow == 2) || (absCol == 2 && absRow == 1) || p == pos)
      }
    }
  }
}