package io.scalac.chess

import io.scalac.chess.figure._
import scala.collection.GenSeq
import scala.collection.JavaConversions.mapAsScalaConcurrentMap

class Chess(val board: Board) {

  private val solutions = new java.util.concurrent.ConcurrentHashMap[Chessboard,Unit]()

  private def isBig(list: List[Position]): Boolean = list.isDefinedAt(25)

  private def par: List[Position] ⇒ GenSeq[Position] = pos ⇒ if(isBig(pos)) pos.par else pos

  def chessboard(chessmen: List[Figure]): Set[Chessboard] = {

    def placeFigure(chessmen: List[Figure], positions: List[Position],
                    currentBoard: Chessboard = Chessboard.empty): Unit = {
      if (chessmen.isEmpty) solutions.put(currentBoard, Unit)
      else {
        par(positions).map(Chessman(_, chessmen.head)).filter(_.isNoConflict(currentBoard))
        .foreach { chessman ⇒
          placeFigure(chessmen.tail, chessman.safePositions(positions), currentBoard + chessman)
        }
      }
    }
    placeFigure(chessmen, board.allPositions)
    solutions.to[Set].map(_._1)
  }

  def show(chess: Chessboard): String = {

    def setChessboard(chesses: Chessboard): Vector[Vector[String]] =
      if(chesses.isEmpty) board.empty
      else {
        val board = setChessboard(chesses.tail)
        val Chessman(pos, figure) = chesses.head
        board.updated(pos.row, board(pos.row).updated(pos.col, figure.sign.toString))
      }
    "\n" + (setChessboard(chess).map(_ mkString " ") mkString "\n")
  }
}
