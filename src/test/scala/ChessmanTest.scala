import _root_.io.scalac.chess.figure.King
import _root_.io.scalac.chess.{Board, Chessman, Position}
import org.scalatest.{Matchers, FunSuite}

class ChessmanTest extends FunSuite with Matchers {

  test("for one King in the corner of board 3x3 we have 5 safe positions ") {
    val king = Chessman(Position(0,0), King)
    val safePositions = Position(2,0) :: Position(2,1) :: Position(2,2) :: Position(1,2) :: Position(0,2) :: Nil
    val board = new Board(3)
    king.safePositions(board.allPositions) should contain only (safePositions: _*)
  }

}
