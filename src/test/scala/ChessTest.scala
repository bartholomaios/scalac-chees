
import io.scalac.chess._
import org.scalatest.FunSuite
import io.scalac.chess.figure._

class ChessTest extends FunSuite {

  test("Application when have 1 Kings on board 4x4 should have 16 results"){
    val figure = King
    val chess = new Chess(new Board(4))
    val result = chess.chessboard(figure :: Nil)
    assert(result.size == 16)
  }

  test("Application when have 4 Kings on board 4x4 should contains chessboard with King on the corners"){
    val kingsChessboardList = Set(Position(0,0), Position(0,3), Position(3,0), Position(3,3))
    val figure = King
    val fourKings = List.fill(4)(figure)
    val chess = new Chess(new Board(4))
    assert(chess.chessboard(fourKings) contains kingsChessboardList.map(Chessman(_, figure)))
  }

  test("Application when have 4 Bishops on board 4x4 should contains chessboard with Bishops in one horizontal line"){
    val bishopsChessboardList = Set(Position(0,0), Position(0,1), Position(0,2), Position(0,3))
    val figure: Bishop.type = Bishop
    val fourBishops = List.fill(4)(figure)
    val chess = new Chess(new Board(4))
    assert(chess.chessboard(fourBishops) contains bishopsChessboardList.map(Chessman(_, figure)))
  }

  test("Application when have 4 Bishops on board 4x4 should NOT contains chessboard with Bishops on diagonal"){
    val bishopsChessboardList = Set(Position(0,3), Position(1,2), Position(2,1), Position(3,0))
    val figure: Bishop.type = Bishop
    val fourBishops = List.fill(4)(figure)
    val chess = new Chess(new Board(4))
    assert(!(chess.chessboard(fourBishops) contains bishopsChessboardList.map(Chessman(_, figure))))
  }

  test("Application when have 4 Queens on board 4x4 should contains chessboard with Queens like one chesseboard"){
    val queensChessboardList = Set(Position(0,2), Position(1,0), Position(2,3), Position(3,1))
    val figure: Queen.type = Queen
    val fourQueens = List.fill(4)(figure)
    val chess = new Chess(new Board(4))
    assert(chess.chessboard(fourQueens) contains queensChessboardList.map(Chessman(_, figure)))
  }

  test("Application when have 4 Knights on board 4x4 should contains chessboard with Knights in one vertical line"){
    val knightsChessboardList = Set(Position(0,1), Position(1,1), Position(2,1), Position(3,1))
    val figure: Knight.type = Knight
    val fourKnights = List.fill(4)(figure)
    val chess = new Chess(new Board(4))
    assert(chess.chessboard(fourKnights) contains knightsChessboardList.map(Chessman(_, figure)))
  }

  test("Application when have 4 Rook on board 4x4 should contains chessboard with Rooks on diagonal"){
    val rooksChessboardList = Set(Position(0,0), Position(1,1), Position(2,2), Position(3,3))
    val figure: Rook.type = Rook
    val fourRooks = List.fill(4)(figure)
    val chess = new Chess(new Board(4))
    assert(chess.chessboard(fourRooks) contains rooksChessboardList.map(Chessman(_, figure)))
  }

  test("Application when have 2 Rook and 4 Knights on board 4x4 should have chessboards "){
    val chess = new Chess(new Board(4))
    val chessboard = Set(Position(0,0) -> Knight, Position(0,2) -> Knight, Position(2,0) -> Knight, Position(2,2) -> Knight,
        Position(1,3) -> Rook, Position(3,1) -> Rook).map(t => Chessman(t._1, t._2))
    val chessman = List.fill(4)(Knight) ++ List.fill(2)(Rook)
    val result = chess.chessboard(chessman)
    assert(result contains chessboard)
    assert(result.size === 8)
  }
  test("Application when have 1 Rook and 2 King on board 3x3 should have chessboards ") {
    val chess = new Chess(new Board(3))
    val chessboard = Set(Position(0, 0) -> King, Position(0, 2) -> King, Position(2, 1) -> Rook).map(t => Chessman(t._1, t._2))
    val chessman = List.fill(2)(King) :+ Rook
    val result = chess.chessboard(chessman)
    assert(result contains chessboard)
    assert(result.size === 4)
  }
  test("Application when have 1 Rook and 1 King on board 3x3 should have chessboards ") {
    val chess = new Chess(new Board(3))
    val chessboard = Set(Position(0, 0) -> King, Position(2, 1) -> Rook).map(t => Chessman(t._1, t._2))
    val chessman = List.fill(1)(King) :+ Rook
    val result = chess.chessboard(chessman)
    assert(result contains chessboard)
    assert(result.size === 20)
  }
  test("Application when have 2 King, 1 Queen, 1 Bishop and 1 Knight on board 5x5 should have some results"){
    val dim = 5
    val chess = new Chess(new Board(dim))
    val chessman = List.fill(2)(King) :+ Queen :+ Bishop :+ Knight
    val result = chess.chessboard(chessman)
    val maxResult = {
      val max = dim*dim
      max*(max-1)*(max-2)*(max-3)
    }
    println("RESULT is " + result.size + " / " + maxResult)
    assert(result.size < maxResult)
  }
  test("Application when have 2 King, 2 Queens, 1 Bishop and 1 Knight on board 6x6 should have some results"){
    val dim = 6
    val chess = new Chess(new Board(dim))
    val chessman = List.fill(2)(King) ++ List.fill(2)(Queen) :+ Bishop :+ Knight
    val result = chess.chessboard(chessman)
    val maxResult = {
      val max = dim*dim
      max*(max-1)*(max-2)*(max-3)
    }
    println("RESULT is " + result.size + " / " + maxResult)
    assert(result.size < maxResult)
  }
  test("Application when have 2 King, 2 Queens, 2 Bishops and 1 Knight on board 7x7 should have some results"){
    val dim = 7
    val chess = new Chess(new Board(dim))
    val chessman = List.fill(2)(King) ++ List.fill(2)(Queen) ++ List.fill(2)(Bishop) :+ Knight
    val result = chess.chessboard(chessman)
    val maxResult = {
      val max = dim*dim
      max*(max-1)*(max-2)*(max-3)
    }
    println("RESULT is " + result.size + " / " + maxResult)
    assert(result.size < maxResult)
  }
}
