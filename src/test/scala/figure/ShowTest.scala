package figure


import io.scalac.chess.figure._
import io.scalac.chess._
import org.scalatest.FunSuite

class ShowTest extends FunSuite {

  test("Application when have 4 Kings should show chessboard with King on the corners"){
   val fourKings = List.fill(4)(King)
   val chess = new Chess(new Board(4))
   val kingsChessboard = """
K * * K
* * * *
* * * *
K * * K"""
    assert(chess.chessboard(fourKings).map(chess.show) contains kingsChessboard )
  }
}
