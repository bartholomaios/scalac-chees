package figure

import io.scalac.chess.Position
import io.scalac.chess.figure._
import org.scalatest.FunSuite

class FigureTest extends FunSuite {

   test("King shoul generate danger around him self"){
     val fixedPosition = Position(2,2)
     val royalSecurity: Position => Boolean = King.fieldOfDestruction(fixedPosition)
     val unSafePositions = Seq(
       Position(1,1), Position(1,2), Position(1,3),
       Position(2,1), Position(2,2), Position(2,3),
       Position(3,1), Position(3,2), Position(3,3))
     val safePositions = Seq(
       Position(0,0), Position(0,1), Position(0,2), Position(0,3), Position(0,4),
       Position(1,0), Position(1,4),
       Position(2,0), Position(2,4),
       Position(3,0), Position(3,4),
       Position(4,0), Position(4,1), Position(4,2), Position(4,3), Position(4,4))
     assert(!(unSafePositions exists royalSecurity))
     assert(safePositions forall royalSecurity)
   }

  test("Queen shoul generate danger on diagonals, vertical and horizontal"){
    val fixedPosition = Position(2,2)
    val royalSecurity: Position => Boolean = Queen.fieldOfDestruction(fixedPosition)
    val unSafePositions = Seq(
      Position(0,0), Position(0,2), Position(0,4),
      Position(1,1), Position(1,2), Position(1,3),
      Position(2,0), Position(2,1), Position(2,2), Position(2,3), Position(2,4),
      Position(3,1), Position(3,2), Position(3,3),
      Position(4,0), Position(4,2), Position(4,4))
    val safePositions = Seq(
      Position(0,1), Position(0,3),
      Position(1,0), Position(1,4),
      Position(3,0), Position(3,4),
      Position(4,1), Position(4,3))
    assert(!(unSafePositions exists royalSecurity))
    assert(safePositions forall royalSecurity)
  }

  test("Bishop shoul generate danger on diagonals"){
    val fixedPosition = Position(2,2)
    val royalSecurity: Position => Boolean = Bishop.fieldOfDestruction(fixedPosition)
    val unSafePositions = Seq(
      Position(0,0), Position(0,4),
      Position(1,1), Position(1,3),
      Position(2,2),
      Position(3,1), Position(3,3),
      Position(4,0), Position(4,4))
    val safePositions = Seq(
      Position(0,1), Position(0,2), Position(0,3),
      Position(1,0), Position(1,2), Position(1,4),
      Position(2,0), Position(2,1), Position(2,3), Position(2,4),
      Position(3,0), Position(3,2), Position(3,4),
      Position(4,1), Position(4,2), Position(4,3))
    assert(!(unSafePositions exists royalSecurity))
    assert(safePositions forall royalSecurity)
  }

  test("Rook shoul generate danger on vertical and horizontal"){
    val fixedPosition = Position(2,2)
    val royalSecurity: Position => Boolean = Rook.fieldOfDestruction(fixedPosition)
    val unSafePositions = Seq(
      Position(0,2),
      Position(1,2),
      Position(2,0), Position(2,1), Position(2,2), Position(2,3), Position(2,4),
      Position(3,2),
      Position(4,2))
    val safePositions = Seq(
      Position(0,0), Position(0,1), Position(0,3), Position(0,4),
      Position(1,0), Position(1,1), Position(1,3), Position(1,4),
      Position(3,0), Position(3,1), Position(3,3), Position(3,4),
      Position(4,1), Position(4,0), Position(4,4), Position(4,3))
    assert(!(unSafePositions exists royalSecurity))
    assert(safePositions forall royalSecurity)
  }

  test("Knight shoul generate danger on L movement"){
    val fixedPosition = Position(2,2)
    val royalSecurity: Position => Boolean = Knight.fieldOfDestruction(fixedPosition)
    val unSafePositions = Seq(
      Position(0,1), Position(0,3),
      Position(1,0), Position(1,4),
      Position(2,2),
      Position(3,0), Position(3,4),
      Position(4,1), Position(4,3))
    val safePositions = Seq(
      Position(0,0), Position(0,2), Position(0,4),
      Position(1,1), Position(1,2), Position(1,3),
      Position(2,0), Position(2,1), Position(2,3), Position(2,4),
      Position(3,1), Position(3,2), Position(3,3),
      Position(4,0), Position(4,2), Position(4,4))
    assert(!(unSafePositions exists royalSecurity))
    assert(safePositions forall royalSecurity)
  }
 }
